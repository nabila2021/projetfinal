# Localisateurs

# Menu en-t�te
link_my_account="xpath://span[text()='My Account']"
link_register="xpath://a[text()='Register']"

# Enregistrement
txt_firstname="id=input-firstname"
#txt_firstname="xpath://input[@id='input-firstname']"
txt_lastname="id=input-lastname"
txt_email="id=input-email"
txt_telephone="id=input-telephone"
txt_password="id=input-password"
txt_password_confirm="id=input-confirm"
radioBtn_newsletter="newsletter" 
checkbox_agree="name=agree"   
btn_continue_register="xpath://input[@value='Continue']"
txt_register_confirm_message="xpath://h1[text()='Your Account Has Been Created!']"

# D�connexion
link_logout="xpath://a[text()='Logout']"

# Message already registered
txt_email_already_registered="xpath=//div[@class='alert alert-danger alert-dismissible']"

# Login
Txt_Password_Forgot="//a[text()='Forgotten Password']"
TXt_Alert="//div[@class='alert alert-success alert-dismissible'] "

# Search product
search_Products="//input[@name='search']"
Btn_Search="//button[@class='btn btn-default btn-lg']"
Txt_Product_NonDisponible="//p[text()='There is no product that matches the search criteria.'] "
btn_Add_To_Cart="//span[text()='Add to Cart']"
btn_Checkout="//span[text()='Checkout']"



MY_Account="//span[text()='My Account']"
txt_Input_Email="//input[@id='input-email']"
txt_Input_Pass="//input[@id='input-password']"
btn_Login="//input[@value='Login']"
btn_MenuLogin="//a[text()='Login']"


Txt_Forget_Password="//a[text()='Forgotten Password']"
Alert_Message="//div[@class='alert alert-success alert-dismissible']"

txt_Search="//input[@name='search']"
btn_search="//button[@class='btn btn-default btn-lg']"
Alert_Message_pas_De_Produit="//p[text()='There is no product that matches the search criteria.']"


btn_continue_forgotPassword="//input[@value='Continue']"

Name_Product="//a[text()='Samsung SyncMaster 941BW']"
Name_Product_NotFound="//p[text()='There is no product that matches the search criteria.']"


# Commande de produits
#txt_area_search="//input[@name='search']"
#btn_search="//button[@class='btn btn-default btn-lg']"
#link_produit="//a[.='Samsung SyncMaster 941BW']"
link_produit="//a[.='Nikon D300']"

btn_Add_cart="//button[@id='button-cart']"
valider_add="//div[@class='alert alert-success alert-dismissible']"
link_shopping_cart="//a[@title='Shopping Cart']"
btn_Checkout="//a[.='Checkout']"
text_area_email="//div/input[@name='email']"
text_area_pass="//input[@id='input-password']"
btn_Login_commande="//input[@id='button-login']"
btn_continue_etape_deux="//input[@id='button-payment-address']"
btn_continue_etape_trois="//input[@id='button-shipping-address']"
btn_continue_etape_quatre="//input[@id='button-shipping-method']"
btn_continue_etape_cinq="//input[@id='button-payment-method']"
btn_confirm_commande="//input[@id='button-confirm']"
btn_checkbox="//input[@name='agree']"