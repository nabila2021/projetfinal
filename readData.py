# pour ouvrir fichier excel importer librairie
import openpyxl
# charger le fichier excel pour pouvoir le manipuler
wk = openpyxl.load_workbook("G:\\B de B\\03_Ete 2021\\1_Automatisation 1\\Eclipse\\A02_ProjetFinal\\TestData\\A02_ProjetFinal_datapool.xlsx")

#créer une méthode pour trouver le nombre maximum de lignes
def fetch_number_of_rows(sheetName):
    sh = wk[sheetName]
    return sh.max_row

# fonction qui retourne une valeur de cellule
def fetch_cell_data(sheetName,row,cell):
    sh = wk[sheetName]
    cell = sh.cell(int(row),int(cell))
    return cell.value