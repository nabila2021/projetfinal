*** Settings ***
Documentation     Fichiers de scripts
Library    SeleniumLibrary
Library    DateTime
Resource    ../Ressources/Keywords/keywords.robot
Variables    ../Ressources/Locators/locators.py

*** Variables ***
${vURL}    http://tutorialsninja.com/demo/index.php?route=common/home   
${vBrowser}    chrome
${vPrenom}    Prenom   
${vNom}    Nom
${vEmail}    prenom.nom@gmail.com
${vTelephone}    5141234567
${vPassword}    12345678
${vPasswordConfirm}    12345678
${vNewsletterNo}    0
${vNewsletterYes}    1

*** Test Cases ***
RQ1.BF1.CT1 Enregistrer un nouveau compte avec des données valides
    #### Variables ####
    ${vCompletedDate}=    Init Date

    #### Préconditions ####
    Ouvrir le site web Your Store    ${vURL}    ${vBrowser}

    #### Cas de test ####
    Click Element    ${link_my_account}    
    Click Element    ${link_register}    
    Input Text    ${txt_firstname}    ${vPrenom}
    Input Text    ${txt_lastname}    ${vNom}${vCompletedDate}
    Input Text    ${txt_email}    ${vEmail}${vCompletedDate}
    Input Text    ${txt_telephone}    ${vTelephone}
    Input Password    ${txt_password}    ${vPassword}    
    Input Password    ${txt_password_confirm}    ${vPasswordConfirm}
    Select Radio Button    ${radioBtn_newsletter}    ${vNewsletterNo}   
    Select Checkbox    ${checkbox_agree}   
    Click Button   ${btn_continue_register}
        
    #### Critère de succès ####    
    #### Valider le message d'enregistrement compléter ####
    Element Text Should Be    ${txt_register_confirm_message}    Your Account Has Been Created!   
    #### Valider le titre de la page ####    
    ${Titrepage}=    Get Title
    Should Be Equal As Strings    ${Titrepage}    Your Account Has Been Created!    

    #### Postconditions ####
    Fermer le navigateur

RQ1.BF1.CT2 Enregistrer un nouveau compte avec des données valides et un abonnement Newletter
    #### Variables ####
    ${vCompletedDate}=    Init Date
    
    #### Préconditions ####
    Ouvrir le site web Your Store    ${vURL}    ${vBrowser}

    #### Cas de test ####
    ${row}=    read number of rows    data
    # 2 car on ne veut pas l'entête ; row + 1 car la boucle ne tient pas compte du dernier élément
    FOR    ${i}    IN RANGE    2    ${row}+1
        ${vPrenom}=    read excel data of cell    data    ${i}    1
        ${vNom}=    read excel data of cell    data    ${i}    2
        ${vEmail}=    read excel data of cell    data    ${i}    3
        ${vTelephone}=    read excel data of cell    data    ${i}    4
        ${vPassword}=    read excel data of cell    data    ${i}    5
        ${vPasswordConfirm}=    read excel data of cell    data    ${i}    6
        Click Element    ${link_my_account}    
        Click Element    ${link_register}    
        Input Text    ${txt_firstname}    ${vPrenom}
        Input Text    ${txt_lastname}    ${vNom}
        Input Text    ${txt_email}    ${vEmail}${vCompletedDate}
        Input Text    ${txt_telephone}    ${vTelephone}
        Input Password    ${txt_password}    ${vPassword}    
        Input Password    ${txt_password_confirm}    ${vPasswordConfirm}
        Select Radio Button    ${radioBtn_newsletter}    ${vNewsletterYes}   
        Select Checkbox    ${checkbox_agree} 
        Click Button   ${btn_continue_register}
        #### Critère de succès ####    
        #### Valider le message d'enregistrement compléter ####
        Element Text Should Be    ${txt_register_confirm_message}    Your Account Has Been Created!   
        #### Valider le titre de la page ####    
        ${Titrepage}=    Get Title
        Should Be Equal As Strings    ${Titrepage}    Your Account Has Been Created!    
        ## Se déconnecter pour refaire l'enregistrement avec un autre record
        Se deconnecter
    END        

    #### Postconditions ####
    Fermer le navigateur
 
RQ1.BF1.CT3 Utilisateur ne peut pas enregistrer en double
    #### Variables ####
    ${vCompletedDate}=    Init Date
    
    #### Préconditions ####
    Ouvrir le site web Your Store    ${vURL}    ${vBrowser}
    Click Element    ${link_my_account}    
    Click Element    ${link_register}    
    Input Text    ${txt_firstname}    ${vPrenom}
    Input Text    ${txt_lastname}    ${vNom}
    Input Text    ${txt_email}    ${vEmail}${vCompletedDate}
    Input Text    ${txt_telephone}    ${vTelephone}
    Input Password    ${txt_password}    ${vPassword}    
    Input Password    ${txt_password_confirm}    ${vPasswordConfirm}
    Select Radio Button    ${radioBtn_newsletter}    ${vNewsletterNo}   
    Select Checkbox    ${checkbox_agree}   
    Click Button   ${btn_continue_register}
    Se deconnecter
    
    #### Cas de test ####
    Click Element    ${link_my_account}    
    Click Element    ${link_register}    
    Input Text    ${txt_firstname}    ${vPrenom}
    Input Text    ${txt_lastname}    ${vNom}
    Input Text    ${txt_email}    ${vEmail}${vCompletedDate}
    Input Text    ${txt_telephone}    ${vTelephone}
    Input Password    ${txt_password}    ${vPassword}    
    Input Password    ${txt_password_confirm}    ${vPasswordConfirm}
    Select Radio Button    ${radioBtn_newsletter}    ${vNewsletterNo}   
    Select Checkbox    ${checkbox_agree}   
    Click Button   ${btn_continue_register}
        
    #### Critère de succès ####    
    #### Valider le message que le courriel existe déjà ####
    Element Text Should Be    ${txt_email_already_registered}    Warning: E-Mail Address is already registered!   
    #### Valider le titre de la page ####    
    ${Titrepage}=    Get Title
    Should Be Equal As Strings    ${Titrepage}    Register Account    

    #### Postconditions ####
    Fermer le navigateur


RQ1.BF2.CT1 L'utilisateur peut se connecter avec des informations d'identification valides
    #### Variables ####
    ${vCompletedDate}=    Init Date
           
    #### Préconditions ####
    Ouvrir le site web Your Store    ${vURL}    ${vBrowser}
    Click Element    ${link_my_account}    
    Click Element    ${link_register}    
    Input Text    ${txt_firstname}    ${vPrenom}
    Input Text    ${txt_lastname}    ${vNom}
    Input Text    ${txt_email}    ${vEmail}${vCompletedDate}
    Input Text    ${txt_telephone}    ${vTelephone}
    Input Password    ${txt_password}    ${vPassword}    
    Input Password    ${txt_password_confirm}    ${vPasswordConfirm}
    Select Radio Button    ${radioBtn_newsletter}    ${vNewsletterNo}   
    Select Checkbox    ${checkbox_agree}   
    Click Button   ${btn_continue_register}
    Se deconnecter

    #### Cas de test #### 
    Click Element    ${link_my_account}   
    Click Element    ${btn_MenuLogin}    
    Input Text    ${txt_Input_Email}    ${vEmail}${vCompletedDate}    
    Input Password    ${txt_Input_Pass}    ${vPassword}
    Click Element    ${btn_Login} 
    #### Critère de succès ####  
    #### Valider le message my Acocount s'apparait ####
    Element Should Be Visible    //h2[text()='My Account']
    #### Postconditions ####
    Fermer le navigateur

RQ1.BF2.CT2 l'utilisateur est en mesure de réinitialiser le mot de passe oublié
    #### Variables ####
    ${vCompletedDate}=    Init Date

    #### Préconditions ####
    Ouvrir le site web Your Store    ${vURL}    ${vBrowser}
    Click Element    ${link_my_account}    
    Click Element    ${link_register}    
    Input Text    ${txt_firstname}    ${vPrenom}
    Input Text    ${txt_lastname}    ${vNom}
    Input Text    ${txt_email}    ${vEmail}${vCompletedDate}
    Input Text    ${txt_telephone}    ${vTelephone}
    Input Password    ${txt_password}    ${vPassword}    
    Input Password    ${txt_password_confirm}    ${vPasswordConfirm}
    Select Radio Button    ${radioBtn_newsletter}    ${vNewsletterNo}   
    Select Checkbox    ${checkbox_agree}   
    Click Button   ${btn_continue_register}
    Se deconnecter

    #### Cas de test #### 
    Click Element    ${link_my_account}   
    Click Element    ${btn_MenuLogin}    
    Click Element    ${Txt_Forget_Password} 
    Input Text    ${txt_Input_Email}    ${vEmail}${vCompletedDate}    
    Click Element    ${btn_continue_forgotPassword}     

    #### Critère de succès ####  
    #### Valider le message : An email with a confirmation link has been sent your email address. ####
    Element Should Be Visible    ${Alert_Message}

    #### Postconditions ####
    Fermer le navigateur

RQ2.BF1.CT1 l'utilisateur est en mesure de rechercher des produits
    #### Variables ####

    #### Préconditions ####
    Ouvrir le site web Your Store    ${vURL}    ${vBrowser}
    
    #### Cas de test #### 
    Input Text    ${txt_Search}    Samsung
    Click Element    ${btn_search}     

    #### Critère de succès ####  
    #### Valider la présence d'un produit Samsung
    Element Should Be Visible    ${Name_Product}

    #### Postconditions ####
    Fermer le navigateur    

RQ2.BF1.CT2 l'utilisateur est informé lorsque le produit recherché n'est pas disponible
    #### Variables ####

    #### Préconditions ####
    Ouvrir le site web Your Store    ${vURL}    ${vBrowser}
    
    #### Cas de test #### 
    Input Text    ${txt_Search}    SamsungWW
    Click Element    ${btn_search}     

    #### Critère de succès ####  
    #### Valider message d'erreur de produit SamsungWW 
    Element Should Be Visible    ${Txt_Product_NonDisponible}

    #### Postconditions ####
    Fermer le navigateur    

RQ3.BF1.CT1 Commande des produits
    Given Ouvrir le site web Your Store    ${vURL}    ${vBrowser}
    And je lance la recherche d un produit
    And J'ajoute le produit au panier
    And Acceder au panier et valider la commande
    And Saisir les informations necessaires
    Then Valider que la commande est passée
    And Se deconnecter
    And Fermer le navigateur