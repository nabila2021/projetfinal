*** Settings ***
Library    SeleniumLibrary    
Library    DateTime
Library    String
Library    ../../TestData/readData.py
Variables    ../Locators/locators.py
*** Variables ***
${TIMEOUT}       10s

*** Keywords ***
Ouvrir le site web Your Store
    [Arguments]    ${vURL}    ${vBrowser}
    # Definir la valeur de timeout pour le cas de test
    Set Selenium Timeout    ${TIMEOUT}
    # Ouvrir le navigateur en precisant l'URL et le navigateur
    Open Browser    ${vURL}    ${vBrowser}
    # Maximiser la fenetre du navigateur
    Maximize Browser Window

Se deconnecter
    Click Element    ${link_my_account}    
    Click Element    ${link_logout}    

Fermer le navigateur
    Close Browser
    
Init Date
    ${vDateSys}=    Get Current Date    exclude_millis=yes
    ${vDate}=     Get Substring    ${vDateSys}    0    10
    ${vHeure}=     Get Substring    ${vDateSys}    11    13
    ${vMinute}=     Get Substring    ${vDateSys}    14    16
    ${vSeconde}=     Get Substring    ${vDateSys}    17    19
    ${vCompleteDate}=     catenate    ${vDate}-${vHeure}${vMinute}${vSeconde}
    [return]    ${vCompleteDate}

read number of rows
    [Arguments]    ${sheetName}
    # la fonction prend comme argument le nom de la feuille et retourne le maximun de lignes
    ${max_row}=    fetch_number_of_rows    ${sheetName}
    [Return]  ${max_row}
    
read excel data of cell
    [Arguments]    ${sheetName}    ${row}    ${cell}
    ${setData}=    fetch_cell_data    ${sheetName}    ${row}    ${cell}
    [Return]    ${setData}
        
je lance la recherche d un produit
    [Documentation]    ce mot cle à lancer la recherche pour le produit Samsung
    Input Text    ${txt_Search}    Nikon D300
    Click Button    ${btn_search}

J'ajoute le produit au panier
    Click Link    ${link_produit}
    Click Button    ${btn_Add_cart}
    
Acceder au panier et valider la commande
    Sleep    2
    Click Element    ${link_shopping_cart}
    Click Element    ${btn_Checkout}
    sleep    2
    Input Text    ${text_area_email}    2119755@bdeb.qc.ca
    Input Text    ${text_area_pass}    12345678
    Click Element    ${btn_Login_commande}
    
Saisir les informations necessaires
    #Je valide l'étape 3
    sleep    2
    Click Element    ${btn_continue_etape_deux}
    #Je valide l'étape 4
    sleep    2
    Click Element    ${btn_continue_etape_trois}
    #Je valide l'étape 5
    sleep    2
    Click Element    ${btn_continue_etape_quatre}
    #Cocher la case
    sleep    2
    Select Checkbox    ${btn_checkbox}
    #Valider payement method
    Click Element    ${btn_continue_etape_cinq}
    #Confirmer l'ordre
    sleep    2
    Click Element    ${btn_confirm_commande}
    sleep    2    

Valider que la commande est passée
    Page Should Contain    Your order has been placed!